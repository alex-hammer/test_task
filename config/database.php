<?php

return [
    'paths' => [
        'migrations' => 'database/migrations'
    ],
    'migration_base_class' => '\App\Database\Migration',
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'dev',
        'dev' => [
            'adapter' => 'sqlite',
        ]
    ]
];
