<?php

declare(strict_types=1);

use GuzzleHttp\Psr7\ServerRequest;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;

require dirname(__DIR__) . '/vendor/autoload.php';

$router = require_once dirname(__DIR__) . '/routes/web.php';


$response = $router->dispatch(ServerRequest::fromGlobals());

(new SapiEmitter)->emit($response);
