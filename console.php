#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use App\Console\FirstCommand;
use Illuminate\Container\Container;

$container = Container::getInstance();
$application = new Application();

$commands  = [
    FirstCommand::class,
];

foreach ($commands as $class) {
    $command = new $class();
    $application->add($command);
}

$application->run();
