<?php

declare(strict_types=1);

namespace App\Http\Controllers;

class Home
{
    public function index(): string
    {
        return '<h1>Home!</h1>';
    }
}
