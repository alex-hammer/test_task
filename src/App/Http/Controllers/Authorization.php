<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use App\Http\Middleware\Auth as AuthMiddleware;

class Authorization
{
    public function __invoke(): ResponseInterface
    {
        if (AuthMiddleware::isAuthorized()) {
            return new Response(302, ['location' => '/']);
        }

        return new Response(body: '<h1>Auth Form</h1>');
    }
}
