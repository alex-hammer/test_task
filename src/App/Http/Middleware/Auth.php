<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;

class Auth implements MiddlewareInterface
{
    public const LOGIN_PAGE_URI = '/login';

    /**
     * {@inheritdoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $auth = $this::isAuthorized();

        if ($auth === true) {

            return $handler->handle($request);
        }

        return new Response(302, ['location' => static::LOGIN_PAGE_URI]);
    }

    // TODO remove it. use something like session manager
    public static function isAuthorized(): bool
    {
        return true;
    }
}
