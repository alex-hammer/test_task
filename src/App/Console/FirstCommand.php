<?php

declare(strict_types=1);

namespace App\Console;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FirstCommand extends BaseCommand
{
    protected static $defaultName = 'first command';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('test first command');

        return 0;
    }
}
