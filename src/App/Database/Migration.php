<?php

declare(strict_types=1);

namespace App\Database;

use Illuminate\Database\Capsule\Manager as Capsule;
use Phinx\Migration\AbstractMigration;
use Illuminate\Database\Schema\Builder;

class Migration extends AbstractMigration
{
    public Capsule $capsule;
    public Builder $schema;

    // TODO подыскать более легкий мигратор
    public function init()
    {
        $this->capsule = new Capsule;
        // TODO брать из конфига
        $this->capsule->addConnection([
            'driver'    => 'sqlite',
            'database' => env('DB_DATABASE', '/database/database.sqlite'),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ]);

        $this->capsule->bootEloquent();
        $this->capsule->setAsGlobal();
        $this->schema = $this->capsule->schema();
    }
}
