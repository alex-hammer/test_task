<?php

declare(strict_types=1);

namespace Framework\Http\Router;

use ReflectionClass;
use League\Route\Route;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Support\Arr;
use League\Route\RouteGroup;
use InvalidArgumentException;
use GuzzleHttp\Psr7\HttpFactory;
use League\Route\RouteCollectionTrait;
use League\Route\Router as LeagueRouter;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @method RouteGroup group(string $prefix, callable $group)
 * @method Route get(string $path, $handler): Route
 */
class LeagueRouterDecorator
{
    /**
     * @param LeagueRouter $leagueRouter
     */
    public function __construct(private LeagueRouter $leagueRouter)
    {
    }

    /**
     * @param $method
     * @param $params
     * @return ResponseInterface|mixed
     */
    public function __call($method, $params)
    {
        if (in_array(mb_strtolower($method), $this->availableMethods(), true)) {
            $this->replaceHandler($method, $params);
        }

        return $this->leagueRouter->$method(...$params);
    }

    /**
     * @param $method
     * @param $params
     * @return void
     */
    private function replaceHandler($method, &$params)
    {
        $argNum = ('map' === $method) ? 2 : 1;
        $handler = Arr::get($params, $argNum);
        $handler = $this->prepareHandler($handler);

        $newHandler = function (ServerRequestInterface $request) use ($handler): ResponseInterface {
            $result = $handler($request);
            if (is_a($result, ResponseInterface::class)) {

                return $result;
            }
            $response = (new HttpFactory)->createResponse();

            return $response->withBody(Utils::streamFor($result));
        };

        $params[$argNum] = $newHandler;
    }

    /**
     * @param $handler
     * @return array|callable|string
     */
    private function prepareHandler($handler): array|callable|string
    {
        $result = $handler;

        if (is_string($handler) && class_exists($handler)) { // invokable class
            $result = new $handler;
        }

        if (is_array($handler) && is_string(Arr::get($handler, 0))) {
            $handler[0] = new $handler[0];

            $result = $handler;
        }

        if (!is_callable($result, true)) {
            throw new InvalidArgumentException('Handler is not callable');
        }

        return $result;
    }

    /**
     * @return array
     */
    private function availableMethods(): array
    {
        $reflection = new ReflectionClass(RouteCollectionTrait::class);

        return array_column($reflection->getMethods(), 'name');
    }
}
