<?php

declare(strict_types=1);

use App\Http\Controllers\Home;
use App\Http\Controllers\Profile;
use League\Route\Router as LeagueRouter;
use App\Http\Controllers\Authorization;
use App\Http\Middleware\Auth as AuthMiddleware;
use Framework\Http\Router\LeagueRouterDecorator;

$router = new LeagueRouterDecorator(new LeagueRouter);

$router->get('/', [Home::class, 'index'])->middleware(new AuthMiddleware);

$router->get('/profile', Profile::class)->middleware(new AuthMiddleware);

$router->get(AuthMiddleware::LOGIN_PAGE_URI, Authorization::class);

return $router;
